# -*- coding: utf-8 -*-
"""

Draft python script to read and view lightfield images
Created on Wed Dec  1 20:59:37 2021

@author: Elian
"""


# %%
import os
import numpy as np
import lightfield as lf
from matplotlib import pyplot as plt
from skimage import img_as_float

from joblib import Parallel, delayed

# %% Set light field information
path = os.path.join("lightfields", "greek")
ext = "png"

# %% Read light field
images = lf.utils.to_float(lf.io.lfread(path, ext))

start = [0, 0, 0, 0, 0]
stop = images.shape
# step = [1, 1, 1, 1, 1]
step = [2, 2, 2, 2, 1]
# step = [8, 8, 1, 1, 1]

images = lf.utils.subsample(images, start, stop, step)
images = lf.utils.rgb2gray(images)

# lf.io.lfsave(lf.utils.to_ubyte(images), "lightfields/greek_subsampled", ext)

block_shape = (images.shape[0], images.shape[1], 32, 32, 1)
image_blocks = lf.utils.split(images, block_shape)

# %%
k = 1
alpha = 1
numiter = 20
order = 1

# %% Perform Homography/Affine Low Rank Approximation
# lf.utils.hlra(images)
# lf.utils.llra(images)

# %% Perform Homography/Affine Low Rank Approximation on blocks

res = [lf.algorithms.llra(image_block,k=k,alpha=alpha,numiter=numiter,experiment_name=f'experiments/llra_{i}')\
       for i,image_block in enumerate(image_blocks)]

# result = Parallel(n_jobs=8)(
#     delayed(lambda i,image_block: lf.algorithms.llra(image_block,k=k,alpha=alpha,numiter=numiter,experiment_name=f'experiments/llra_{i}'))(i,image_block)
#     for i,image_block in enumerate(image_blocks)
# )

# %%
print("End of program")
