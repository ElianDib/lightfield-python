# -*- coding: utf-8 -*-
"""
Collection of Light fields utility functions.
#TODO: Use -3 as color channel
#TODO: Prefer use of views when possible
#TODO: Optimize J update
#TODO: Try using diff for faster gradient approximation

"""

import os
import numpy as np
from matplotlib import pyplot as plt
from skimage import img_as_float, img_as_ubyte
from skimage.exposure import rescale_intensity
from skimage.transform import warp as wp
from scipy.sparse.linalg import svds
from scipy.linalg import svd, lstsq, inv
from skimage.util import view_as_blocks as vab, view_as_windows as vaw
from skimage.color import rgb2ycbcr


def get_dir(path, ext="png"):
    """Return list of file names in a directory.

    Parameters
    ----------
    path : str
        Path to directory containing images_in representing
        slices of the light field.
    ext : str, optional
        File extension of images_in. The default is 'png'.

    Returns
    -------
    filenames : list of str
        List of all file names with extension 'ext'.

    """
    with os.scandir(path) as it:
        filenames = [entry.name for entry in it if entry.name.endswith(f".{ext}")]

    return filenames


def print_dir(path, ext="png"):
    """Print list of file names in a directory.

    Parameters
    ----------
    path : str
        Path to directory containing images_in representing
        slices of the light field.
    ext : str, optional
        File extension of images_in. The default is 'png'.

    """
    filenames = get_dir(path, ext)

    print(f"\nFound {len(filenames)} '{ext}' files in {path}:\n")
    print(filenames)


def exactly_4d(images):
    """View inputs as arrays with exactly 4 dimensions.

    Parameters
    ----------
    images : ndarray
        Data representation of the light field as a ndarray.
        The different images_in are stored in the first and second dimensions, such
        that a gray-lf is VxUxYxX, an RGB-lf VxUxYxXx3 and an RGBA-lf is VxUxYxXx4.

    Returns
    -------
    res : ndarray
        An array with ``a.ndim == 4``.  Copies are
        avoided where possible, and views with four or more dimensions are
        returned.
    ang_shape  : ndarray
        angular shape
    img_shape  : ndarray
        image shape

    """
    result = np.atleast_3d(images)
    result = result.reshape((-1,) + result.shape[-3:])
    return result, result.shape[0], result.shape[-3:]


def atleast_5d(images):
    """View inputs as arrays with at least 5 dimensions.

    Parameters
    ----------
    images : ndarray (sequence)
        Data representation of the light field as a ndarray.
        The different images_in are stored in the first and second dimensions, such
        that a gray-lf is VxUxYxX, an RGB-lf VxUxYxXx3 and an RGBA-lf is VxUxYxXx4.

    Returns
    -------
    res : ndarray
        An array with ``a.ndim >= 5``.  Copies are
        avoided where possible, and views with five or more dimensions are
        returned.
    ang_shape  : ndarray
        angular shape
    img_shape  : ndarray
        image shape

    """
    result = np.atleast_3d(images)
    if result.ndim == 3:
        result = result[np.newaxis, np.newaxis, ...]
    elif result.ndim == 4:
        result = result[np.newaxis, ...]
    else:
        result = images
    return result, result.shape[:-3], result.shape[-3:]


def views(lf_array, perm=None):
    """Get views along each dimension of the light field.

    Parameters
    ----------
    lf_array : ndarray
        Data representation of the light field as a ndarray.
        The different images_in are stored in the first and second dimensions, such
        that a gray-lf is VxUxYxX, an RGB-lf VxUxYxXx3 and an RGBA-lf is VxUxYxXx4.
    perm: ndarray
        Axis permutation to allow different data visualizations.

    Returns
    -------
    view_yx : ndarray
        view into lf_array as uv stack of yx slices
    view_yv : ndarray
        view into lf_array as ux stack of yv slices
    view_ux : ndarray
        view into lf_array as vy stack of ux slices
    view_uv : ndarray
        view into lf_array as yx stack of uv slices

    """

    lf_array, *_ = atleast_5d(lf_array)

    # Default: no permutation
    if perm is None:
        perm = range(lf_array.ndim)

    # Transpose array to obtain different slice stacks
    view_ = lf_array.transpose(perm)
    view_yx = view_.transpose([0, 1, 2, 3, 4])
    view_yv = view_.transpose([1, 3, 2, 0, 4])
    view_ux = view_.transpose([0, 2, 1, 3, 4])
    view_uv = view_.transpose([2, 3, 1, 0, 4])

    return view_yx, view_yv, view_ux, view_uv


def slices(lf_array, vuyx=None, perm=None):
    """Get views along each dimension of the light field for specific point.

    Parameters
    ----------
    lf_array : ndarray
        Data representation of the light field as a ndarray.
        The different images_in are stored in the first and second dimensions, such
        that a gray-lf is VxUxYxX, an RGB-lf VxUxYxXx3 and an RGBA-lf is VxUxYxXx4.
    vuyx : ndarray, optional
        Point coordinates.
    perm: ndarray, optional
        Axis permutation to allow different data visualizations.

    Returns
    -------
    slice_yx : ndarray
        yx slice at coordinates (u,v)
    slice_yv : ndarray
        yv slice at coordinates (u,x)
    slice_ux : ndarray
        ux slice at coordinates (v,y)
    slice_uv : ndarray
        uv slice at coordinates (y,x)
    """

    # Default: first slice
    if vuyx is None:
        vuyx = [0, 0, 0, 0]

    # Default: no permutation
    if perm is None:
        perm = range(lf_array.ndim)

    # Get slices
    view_yx, view_yv, view_ux, view_uv = views(lf_array, perm)

    # Get slices coordinates
    vuyx_perm = [t for _, t in sorted(zip(perm[:4], vuyx))]
    v, u, y, x = vuyx_perm

    # Get slices
    slice_yx = view_yx[v, u]
    slice_yv = view_yv[u, x]
    slice_ux = view_ux[v, y]
    slice_uv = view_uv[y, x]

    return slice_yx, slice_yv, slice_ux, slice_uv


def matrix(lf_array, perm=None):
    """Get a light field as a matrix.

    Parameters
    ----------
    lf_array : ndarray
        Data representation of the light field as a ndarray.
        The different images_in are stored in the first and second dimensions, such
        that a gray-lf is VxUxYxX, an RGB-lf VxUxYxXx3 and an RGBA-lf is VxUxYxXx4.
    ax : AxesImage, optional
        Image axes to use for plotting. The default is None.
    perm: ndarray, optional
        Axis permutation to allow different data visualizations.
    shape: tuple
        Light field shape.

    Returns
    -------
    matrix : ndarray
        matrix view of the light field

    """

    # Default: no permutation
    if perm is None:
        perm = range(lf_array.ndim)

    # Get view
    image, *_ = views(lf_array, perm)

    # Reshape
    shape = (np.prod(image.shape[:2]), np.prod(image.shape[2:4]), -1)
    shape_flat = (np.prod(image.shape[:2]), -1)
    return image.reshape(shape), image.reshape(shape_flat)


def get_coords(start, stop, step):
    """Compute coordinates and centered coordinates.

    Parameters
    ----------

    start: tuple
        Start index for each dimension
    stop: tuple
        Stop index for each dimension
    step: tuple
        Step for each dimension

    Returns
    -------
    centered_coords: Tuple of 1-d ndarray
        Coordinates centered around middle value
    coords: Tuple of 1-d ndarray
        Coordinates

    """

    ranges = [range(sta, sto, ste) for sta, sto, ste in zip(start, stop, step)]
    slices = [slice(sta, sto, ste) for sta, sto, ste in zip(start, stop, step)]

    mid = [(sto + sta) // 2 for sta, sto in zip(start, stop)]

    coords = np.meshgrid(*ranges, sparse=True)
    coords = [c - m for c, m in zip(coords, mid)]

    return slices, coords


def mean_shift(M, ax=None):
    if ax is None:
        ax = 0

    M_mean = np.mean(M[ax])
    M_shift = M
    M_shift[0] = M_shift[0] - M_mean

    return M_shift, M_mean


def to_ubyte(M):
    M_norm = rescale_intensity(M, out_range=(0.0, 1.0))
    M_norm = img_as_ubyte(M_norm)

    return M_norm


def to_float(M):
    M_norm = rescale_intensity(M, out_range=(0.0, 1.0))
    M_norm = img_as_float(M_norm)

    return M_norm


def warp(
    images_in,
    matrices,
    images_out=None,
    forward=True,
    order=1,
    mode="edge",
    clip=False,
    preserve_range=True,
):
    """Warp light field.

    Parameters
    ----------
    image_in : ndarray
        Input light field.
    image_out : ndarray
        Output light field.
    matrix : 3x3 ndarray
        Matrix transformation in homogeneous coordinates.
    order : Int
        degree of the interpolation polynomial.
    mode: String
        Extrapolation mode.

    Returns
    -------
    image_out : ndarray
        Output light field.

    """

    # Use input as output by default
    if images_out is None:
        images_out = images_in

    # Reshape images for simpler iterations
    images_in, ang_size, img_shape = exactly_4d(images_in)
    images_shape = images_in.shape
    images_out, *_ = exactly_4d(images_out)

    matrices = matrices.reshape((-1, 3, 3))

    # Forward or backward warp each image
    if forward:
        for it, _ in enumerate(images_in):
            images_out[it] = wp(
                images_in[it],
                matrices[it],
                order=order,
                mode=mode,
                clip=clip,
                preserve_range=preserve_range,
            )
    else:
        for it, _ in enumerate(images_in):
            images_out[it] = wp(
                images_in[it],
                inv(matrices[it]),
                order=order,
                mode=mode,
                clip=clip,
                preserve_range=preserve_range,
            )

    # Reshape output images
    images_out = images_out.reshape(images_shape)

    return images_out


def low_rank_approx(matrix, k=1, matrix_lowrank=None):
    """Compute Low Rank Approximation of a matrix.

    Parameters
    ----------

    k: ndarray
        Rank of the approximation
    matrix: ndarray
        Low rank matrix

    Returns
    -------
    matrix_lowrank: ndarray
        Low rank matrix

    """
    # Perform Singular Value Decomposition
    # _, I  = matrix(matrix)
    U, s, Vh = svds(matrix, k=k)

    S = np.zeros((k, k))
    for i in range(k):
        S[i, i] = s[i]

    # Reshape arrays
    matrix_lowrank = U @ S @ Vh

    return matrix_lowrank


def psnr(reference, target, mask=None):
    """Compute Peak Signal-to-Noise Ratio.

    Parameters
    ----------

    reference: ndarray (float)
        Reference array
    target: ndarray (float)
        Target array
    mask: ndarray (boolean)
        Mask of values inside array

    Returns
    -------
    psnr: float
        PSNR value in dB

    """

    if mask is None:
        mask = np.ones_like(reference, dtype=bool)

    mse = np.mean((reference - target)[mask] ** 2)
    return 10 * np.log10(1 / mse)


def coeffs_to_matrices(coeffs, v, u, matrices=None):
    if matrices is None:
        matrices = np.zeros_like(coeffs, shape=(v.size * u.size, 3, 3))

    V, U = np.meshgrid(v, u, indexing="ij", copy=True, sparse=False)
    V, U = V.reshape(-1, 1), U.reshape(-1, 1)

    matrices[...] = np.identity(3)
    matrices[:, 0] += coeffs[:, :3] * U
    matrices[:, 1] += coeffs[:, 3:] * V

    return matrices

def save_coeffs(coeffs, experiment_name, k=None, alpha=None, numiter=None, order=None):
    """Save coefficients to file.
    
    Parameters
    ----------
    coeffs : ndarray
        Coefficients to save.
    experiment_name : str
        Experiment name.
    k : int
        Rank of the approximation.
    alpha : float
        Regularization parameter.
    numiter : int
        Number of iterations.
    order : int
        Order of the approximation.
    
    """
    
    if not os.path.exists(experiment_name):
        os.makedirs(experiment_name)
    filename = f'{experiment_name}/k_{k:02d}_alpha_{alpha:2.3f}_numiter_{numiter:04d}_order_{order:01d}.coeffs'
    np.save(filename, coeffs)
    print(f"Saved coefficients to {filename}")

def load_coeffs(experiment_name, k=None, alpha=None, numiter=None, order=None):
    """Load coefficients from file.

    Parameters
    ----------
    experiment_name : str
        Experiment name.
    k : int
        Rank of the approximation.
    alpha : float
        Regularization parameter.
    numiter : int
        Number of iterations.
    order : int
        Order of the approximation.

    Returns
    -------
    coeffs : ndarray
        Coefficients loaded from file.

    """

    if not os.path.exists(experiment_name):
        os.makedirs(experiment_name)
    filename = f'{experiment_name}/k_{k:02d}_alpha_{alpha:2.3f}_numiter_{numiter:04d}_order_{order:01d}.coeffs'
    coeffs = np.load(filename)
    print(f"Loaded coefficients from {filename}")
    return coeffs

def save_matrices(matrices, experiment_name, k=None, alpha=None, numiter=None, order=None):
    """Save matrices to file.
    
    Parameters
    ----------
    matrices : ndarray
        Matrices to save.
    experiment_name : str
        Experiment name.
    k : int
        Rank of the approximation.
    alpha : float
        Regularization parameter.
    numiter : int
        Number of iterations.
    order : int
        Order of the approximation.
    
    """

    if not os.path.exists(experiment_name):
        os.makedirs(experiment_name)
    filename = f'{experiment_name}/k_{k:02d}_alpha_{alpha:2.3f}_numiter_{numiter:04d}_order_{order:01d}.matrices'
    np.save(filename, matrices)
    print(f"Saved matrices to {filename}")

def load_matrices(experiment_name, k=None, alpha=None, numiter=None, order=None):
    """Load matrices from file.

    Parameters
    ----------
    experiment_name : str
        Experiment name.
    k : int
        Rank of the approximation.
    alpha : float
        Regularization parameter.
    numiter : int
        Number of iterations.
    order : int
        Order of the approximation.

    Returns
    -------
    matrices : ndarray
        Matrices loaded from file.

    """

    if not os.path.exists(experiment_name):
        os.makedirs(experiment_name)
    filename = f'{experiment_name}/k_{k:02d}_alpha_{alpha:2.3f}_numiter_{numiter:04d}_order_{order:01d}.matrices'
    matrices = np.load(filename)
    print(f"Loaded matrices from {filename}")
    return matrices

def get_grid_coords(images):
    """Get grid coordinates for the light field.

    Parameters
    ----------
    images : ndarray
        Light field images.

    Returns
    -------
    Y : ndarray
        Y coordinates.
    X : ndarray
        X coordinates.
    V : ndarray
        V coordinates.
    U : ndarray
        U coordinates
    
    """

    images, ang_shape, img_shape = atleast_5d(images)
    Y, X, *_ = np.meshgrid(
        *[range(x) for x in img_shape[:2]], indexing="ij", copy=True, sparse=False
    )

    V, U, *_ = np.meshgrid(
        *[range(x) for x in ang_shape[:2]], indexing="ij", copy=True, sparse=False
    )
    return Y, X, V, U

def split(images, block_shape):
    """Split images into blocks.

    Parameters
    ----------
    images : ndarray
        Light field images.
    block_shape : tuple
        Block shape.

    Returns
    -------
    image_blocks : ndarray
        Image blocks.

    """

    image_blocks = vab(images, block_shape)
    return image_blocks.reshape((-1,) + block_shape)

def rgb2gray(images):
    """Convert RGB images to grayscale.

    Parameters
    ----------
    images : ndarray
        Light field images.
    
    Returns
    -------
    images : ndarray
        Grayscale images.

    """

    images = rgb2ycbcr(images)[..., 0] / 255
    return images[..., np.newaxis]

def subsample(images, start, stop, step):
    """Subsample images.

    Parameters
    ----------
    images : ndarray
        Light field images.
    start : tuple
        Start index for each dimension.
    stop : tuple
        Stop index for each dimension.
    step : tuple
        Step for each dimension.

    Returns
    -------
    images : ndarray
        Subsampled images.

    """

    slices, *_ = get_coords(start, stop, step)
    return images[tuple(slices)]

def normalize(images):
    """Normalize images.

    Parameters
    ----------
    images : ndarray
        Light field images.

    Returns
    -------
    images : ndarray
        Normalized images.

    """

    images = images - np.min(images)
    images = images / np.max(images)
    return images