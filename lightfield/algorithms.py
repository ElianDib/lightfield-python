# -*- coding: utf-8 -*-
"""
Collection of Light fields utility functions.
#TODO: Use -3 as color channel
#TODO: Prefer use of views when possible
#TODO: Optimize J update
#TODO: Try using diff for faster gradient approximation

"""

import os
import numpy as np
from skimage.transform import warp as wp
from scipy.sparse.linalg import svds
from scipy.linalg import svd, lstsq, inv
import lightfield.utils as utils
import datetime

def warp(
    images_in,
    matrices,
    images_out=None,
    forward=True,
    order=1,
    mode="edge",
    clip=False,
    preserve_range=True,
):
    """Warp light field.

    Parameters
    ----------
    image_in : ndarray
        Input light field.
    image_out : ndarray
        Output light field.
    matrix : 3x3 ndarray
        Matrix transformation in homogeneous coordinates.
    order : Int
        degree of the interpolation polynomial.
    mode: String
        Extrapolation mode.

    Returns
    -------
    image_out : ndarray
        Output light field.

    """

    # Use input as output by default
    if images_out is None:
        images_out = images_in

    # Reshape images for simpler iterations
    images_in, ang_size, img_shape = utils.exactly_4d(images_in)
    images_shape = images_in.shape
    images_out, *_ = utils.exactly_4d(images_out)

    matrices = matrices.reshape((-1, 3, 3))

    # Forward or backward warp each image
    if forward:
        for it, _ in enumerate(images_in):
            images_out[it] = wp(
                images_in[it],
                matrices[it],
                order=order,
                mode=mode,
                clip=clip,
                preserve_range=preserve_range,
            )
    else:
        for it, _ in enumerate(images_in):
            images_out[it] = wp(
                images_in[it],
                inv(matrices[it]),
                order=order,
                mode=mode,
                clip=clip,
                preserve_range=preserve_range,
            )

    # Reshape output images
    images_out = images_out.reshape(images_shape)

    return images_out


def low_rank_approx(images, k=1, images_lowrank=None):
    """Compute Low Rank Approximation of a light field.

    Parameters
    ----------

    k: ndarray
        Rank of the approximation
    images_lowrank: ndarray
        Low rank matrix

    Returns
    -------
    images_lowrank: ndarray
        Low rank matrix

    """

    # Reshape images for simpler iterations
    images, ang_shape, img_shape = utils.atleast_5d(images)
    images_shape = images.shape
    ang_size, img_size = np.prod(ang_shape), np.prod(img_shape)
    images = images.reshape(ang_size, img_size)
    images_lowrank = images_lowrank.reshape(ang_size, img_size)

    # Compute SVD
    images_lowrank = utils.low_rank_approx(images, k, images_lowrank)

    # Reshape arrays
    images_lowrank = images_lowrank.reshape(images_shape)

    return images_lowrank


def jacobian_homography(images, matrices, J=None, y=None, x=None):
    images, ang_size, img_shape = utils.exactly_4d(images)

    if J is None:
        J = np.zeros_like(images, shape=(ang_size, -1, img_shape[:2]))
    J = J.reshape(
        (
            ang_size,
            -1,
        )
        + img_shape[:2]
    )

    if y is None or x is None:
        y, x = map(np.arange, img_shape[:2])
    y, x = y.reshape(-1, 1), x.reshape(1, -1)

    # Compute jacobian for each image
    for it, image in enumerate(images):
        a, b, c, d, e, f, g, h, _ = matrices[it].ravel()

        U = a * x + b * y + c
        V = d * x + e * y + f
        Wi = np.reciprocal(g * x + h * y + 1)

        # Only compute gradient for first channel (luminance)
        Iy, Ix, *_ = np.gradient(image[..., 0])

        # Compute jacobian
        J[it, 0] = x * Ix * Wi
        J[it, 1] = y * Ix * Wi
        J[it, 2] = Ix * Wi
        J[it, 3] = x * Iy * Wi
        J[it, 4] = y * Iy * Wi
        J[it, 5] = Iy * Wi
        J[it, 6] = -x * (Ix * U + Iy * V) * (Wi**2)
        J[it, 7] = -y * (Ix * U + Iy * V) * (Wi**2)

    return J


def update_delta_homography(targets, images, masks, J, matrices, alpha=1):
    targets, ang_size, _ = utils.exactly_4d(targets)
    images, *_ = utils.exactly_4d(images)
    masks, *_ = utils.exactly_4d(masks)

    # Batch Gradient Descent
    for it, (target, image, mask, matrix_) in enumerate(
        zip(targets, images, masks, matrices)
    ):
        mask_ = mask[..., 0].ravel().astype(dtype="bool")

        # shape: (advanced indexing, slices), no need for transpose
        A = J[it, :, mask_]
        b = (target - image)[..., 0].ravel()[mask_]
        d, *_ = lstsq(A, b)

        matrix_.ravel()[:8] += alpha * d

    return matrices


def hlra(
    images,
    k=1,
    matrices=None,
    masks=None,
    alpha=1,
    numiter=1,
    order=1,
    y=None,
    x=None,
    J=None,
    images_warped=None,
    images_lowrank=None,
    images_recon=None,
    masks_warped=None,
    masks_recon=None,
    verbose=True,
    log=False,
    save=True,
    experiment_name=None
):
    # Initialize default variables
    images, ang_shape, img_shape = utils.atleast_5d(images)
    ang_size = np.prod(ang_shape)

    if experiment_name is None:
        experiment_name = "experiment"
    
    if log:
        timestamp = datetime.datetime.now().strftime("%Y%m%d%H%M%S")
        logsdir = os.path.join(os.getcwd(),'logs')
        if not os.path.exists(logsdir):
            os.makedirs(logsdir)
        logabspath = os.path.join(logsdir, f"{experiment_name}_hlra_{k}_{alpha}_{order}_{numiter}_{timestamp}.log")
        logfile = open(logabspath, "w")

    if images_warped is None:
        images_warped = images.copy()
    if images_lowrank is None:
        images_lowrank = images.copy()
    if images_recon is None:
        images_recon = images.copy()

    if masks is None:
        masks = np.ones_like(images)
    if masks_warped is None:
        masks_warped = masks.copy()
    if masks_recon is None:
        masks_recon = masks.copy()

    if y is None or x is None:
        y, x = map(np.arange, img_shape[:2])
    y, x = y.reshape(-1, 1), x.reshape(1, -1)

    if matrices is None:
        matrices = np.zeros((ang_size,) + (3, 3))
        Id = np.identity(3)
        matrices[...] = Id
    matrices_max = matrices.copy()

    if J is None:
        J = np.zeros_like(images, shape=(ang_size, 8, np.prod(img_shape[:2])))

    # %% Perform HLRA iteratively
    for it in range(numiter):
        # Forward warping
        warp(
            images,
            matrices=matrices,
            images_out=images_warped,
            forward=True,
            order=order,
            mode="edge",
        )
        warp(
            masks,
            matrices=matrices,
            images_out=masks_warped,
            forward=True,
            order=0,
            mode="constant",
        )

        # Low rank approximation
        images_lowrank = low_rank_approx(images_warped, k, images_lowrank)

        psnr_in = utils.psnr(images_warped, images_lowrank, masks_warped != 0)

        # Backward warping
        warp(
            images_lowrank,
            matrices=matrices,
            images_out=images_recon,
            forward=False,
            order=order,
            mode="edge",
        )
        warp(
            masks_warped,
            matrices=matrices,
            images_out=masks_recon,
            forward=False,
            order=0,
            mode="constant",
        )

        psnr_out = utils.psnr(images, images_recon, masks_recon != 0)

        # Update max and argmax
        if it == 0:
            it_max = it
            matrices_max[...] = matrices
            psnr_in_0 = psnr_in
            psnr_out_0 = psnr_out
            psnr_in_max = psnr_in_0
            psnr_out_max = psnr_out_0

        if psnr_out > psnr_out_max:
            it_max = it
            matrices_max[...] = matrices
            psnr_in_max = psnr_in
            psnr_out_max = psnr_out

        # Jacobian computation and homography parameters update
        jacobian_homography(images_lowrank, matrices, J, y, x)
        update_delta_homography(
            images_lowrank, images_warped, masks_warped, J, matrices, alpha=alpha
        )

        if verbose or log:
            iteration_string = f"it:{it:4d}, it_max:{it_max:4d}, psnr_in_max: {psnr_in_max:3.2f} dB ({psnr_in_max-psnr_in_0:+03.2f}), psnr_out_max: {psnr_out_max:3.2f} dB ({psnr_out_max-psnr_out_0:+003.2f})"
        
        if verbose:
            print(iteration_string)
        
        if log:
            logfile.write(iteration_string+"\n")

    if log:
        logfile.close()
    
    if save:
        utils.save_matrices(matrices_max, experiment_name, k, alpha, numiter, order)

    return matrices_max


def jacobian_affine(images, v, u, J, y, x):
    _, ang_shape, img_shape = utils.atleast_5d(images)
    ang_size = np.prod(ang_shape)
    images, *_ = utils.exactly_4d(images)

    if J is None:
        J = np.zeros_like(images, shape=(ang_size, 6, img_shape[:2]))
    J = J.reshape(
        (
            ang_size,
            -1,
        )
        + img_shape[:2]
    )

    if v is None or u is None:
        v, u = map(lambda x: np.arange(x) - x // 2, ang_shape[-2:])

    V, U = np.meshgrid(v, u, indexing="ij", copy=True, sparse=False)
    V, U = V.ravel(), U.ravel()

    if y is None or x is None:
        y, x = map(np.arange, img_shape[:2])
    y, x = y.reshape(-1, 1), x.reshape(1, -1)

    for it, (image, v_it, u_it) in enumerate(zip(images, V, U)):
        # Only compute gradient for first channel (luminance)
        Iy, Ix = np.gradient(image[..., 0])

        J[it, 0] = Ix * u_it * x
        J[it, 1] = Ix * u_it * y
        J[it, 2] = Ix * u_it
        J[it, 3] = Iy * v_it * x
        J[it, 4] = Iy * v_it * y
        J[it, 5] = Iy * v_it

    return J


def update_delta_affine(targets, images, masks, J, coeffs, alpha=1, online=False):
    targets, ang_shape, *_ = utils.exactly_4d(targets)
    images, *_ = utils.exactly_4d(images)
    masks, *_ = utils.exactly_4d(masks)
    ang_size = np.prod(ang_shape)

    if online:
        # Online Gradient Descent (One update per image)
        for it, (target, image, mask) in enumerate(zip(targets, images, masks)):
            mask_ = mask[..., 0].ravel().astype(dtype="bool")

            # shape: (advanced indexing, slices), no need for transpose
            A = J[it, :, mask_]
            b = (target - image)[..., 0].ravel()[mask_]
            d, *_ = lstsq(A, b)

            coeffs += alpha * d / ang_size
    else:
        # Batch Gradient Descent (One update)
        mask_ = masks[..., 0].ravel().astype(dtype="bool")

        J = J.transpose((0, 2, 1)).reshape((-1, 6))
        A = J[mask_, :]
        b = (targets - images)[..., 0].ravel()[mask_]
        d, *_ = lstsq(A, b)

        coeffs += alpha * d

    return coeffs


def llra(
    images,
    k=1,
    coeffs=None,
    masks=None,
    alpha=1,
    numiter=1,
    order=1,
    v=None,
    u=None,
    y=None,
    x=None,
    J=None,
    images_warped=None,
    images_lowrank=None,
    images_recon=None,
    masks_warped=None,
    masks_recon=None,
    verbose=True,
    online=False,
    log=False,
    save=True,
    experiment_name=None
):
    # Initialize default variables
    images, ang_shape, img_shape = utils.atleast_5d(images)
    ang_size = np.prod(ang_shape)

    if experiment_name is None:
        experiment_name = "experiment"
    
    if log:
        timestamp = datetime.datetime.now().strftime("%Y%m%d%H%M%S")
        logsdir = os.path.join(os.getcwd(),'logs')
        if not os.path.exists(logsdir):
            os.makedirs(logsdir)
        logabspath = os.path.join(logsdir, f"{experiment_name}_llra_{k}_{alpha}_{order}_{numiter}_{timestamp}.log")
        logfile = open(logabspath, "w")
        
    if images_warped is None:
        images_warped = images.copy()
    if images_lowrank is None:
        images_lowrank = images.copy()
    if images_recon is None:
        images_recon = images.copy()

    if masks is None:
        masks = np.ones_like(images)
    if masks_warped is None:
        masks_warped = masks.copy()
    if masks_recon is None:
        masks_recon = masks.copy()

    if v is None or u is None:
        v, u = map(lambda x: np.arange(x) - x // 2, ang_shape[-2:])

    if y is None or x is None:
        y, x = map(np.arange, img_shape[:2])
    y, x = y.reshape(-1, 1), x.reshape(1, -1)

    if coeffs is None:
        coeffs = np.zeros((1, 6))
    coeffs_max = coeffs.copy()

    matrices = utils.coeffs_to_matrices(coeffs, v, u)

    if J is None:
        J = np.zeros_like(images, shape=(ang_size, 6, np.prod(img_shape[:2])))

    # %% Perform LLRA iteratively
    for it in range(numiter):
        # Forward warping
        warp(
            images,
            matrices=matrices,
            images_out=images_warped,
            forward=True,
            order=order,
            mode="edge",
        )
        warp(
            masks,
            matrices=matrices,
            images_out=masks_warped,
            forward=True,
            order=0,
            mode="constant",
        )

        # Low rank approximation
        images_lowrank = low_rank_approx(images_warped, k, images_lowrank)

        psnr_in = utils.psnr(images_warped, images_lowrank, masks_warped != 0)

        # Backward warping
        warp(
            images_lowrank,
            matrices=matrices,
            images_out=images_recon,
            forward=False,
            order=order,
            mode="edge",
        )
        warp(
            masks_warped,
            matrices=matrices,
            images_out=masks_recon,
            forward=False,
            order=0,
            mode="constant",
        )

        psnr_out = utils.psnr(images, images_recon, masks_recon != 0)

        # Update max and argmax
        if it == 0:
            it_max = it
            coeffs_max[...] = coeffs
            psnr_in_0 = psnr_in
            psnr_out_0 = psnr_out
            psnr_in_max = psnr_in_0
            psnr_out_max = psnr_out_0

        if psnr_out > psnr_out_max:
            it_max = it
            coeffs_max[...] = coeffs
            psnr_in_max = psnr_in
            psnr_out_max = psnr_out

        # Jacobian computation and homography parameters update
        jacobian_affine(images_lowrank, v, u, J, y, x)
        update_delta_affine(
            images_lowrank,
            images_warped,
            masks_warped,
            J,
            coeffs,
            alpha=alpha,
            online=online,
        )

        utils.coeffs_to_matrices(coeffs, v, u, matrices=matrices)

        if verbose or log:
            iteration_string = f"it:{it:4d}, it_max:{it_max:4d}, psnr_in_max: {psnr_in_max:3.2f} dB ({psnr_in_max-psnr_in_0:+03.2f}), psnr_out_max: {psnr_out_max:3.2f} dB ({psnr_out_max-psnr_out_0:+003.2f})"

        if verbose:
            print(iteration_string)

        if log:
            logfile.write(iteration_string+"\n")
    
    if log:
        logfile.close()

    if save:
        utils.save_coeffs(coeffs_max, experiment_name, k, alpha, numiter, order)
    
    return coeffs_max


# Print formats
# psnr_in_1 = psnr_in_0
# psnr_out_1 = psnr_out_0
# psnr_in_1 = psnr_in
# psnr_out_1 = psnr_out
# d_in_max = psnr_in_max-psnr_in_0
# d_out_max = psnr_out_max-psnr_out_0
# d_in_cur = psnr_in-psnr_in_1
# d_out_cur = psnr_out-psnr_out_1
# print(f"it:{it:4d}, psnr_in(it): {psnr_in:3.2f} dB, psnr_out(it): {psnr_out:3.2f} dB")
# print(f"it_max:{it_max:4d}, psnr_in(it_max): {psnr_in_max:3.2f} dB, psnr_out(it_max): {psnr_out_max:3.2f} dB")
# print(f"it:{it:4d}, it_max:{it_max:4d}, d_in_max: {d_in_max:+3.2f} dB, d_out_max: {d_out_max:+3.2f} dB")
# print(f"it:{it:4d}, it_max:{it_max:4d}, d_in_cur: {d_in_cur:+3.2f} dB, d_out_cur: {d_out_cur:+3.2f} dB")
# print(f"it:{it:4d}, it_max:{it_max:4d}, psnr_in(0): {psnr_in_0:3.2f} dB, psnr_in(it_max): {psnr_in_max:3.2f} dB, d_in_max: {d_in_max:+3.2f} dB")
# print(f"it:{it:4d}, it_max:{it_max:4d}, psnr_out(0): {psnr_out_0:3.2f} dB, psnr_out(it_max): {psnr_out_max:3.2f} dB, d_out_max: {d_out_max:+3.2f} dB")
# print(f"it:{it:4d}, it_max:{it_max:4d}, psnr_in(it): {psnr_in:3.2f} dB, d_in_cur: {d_in_cur:+3.2f} dB")
# print(f"it:{it:4d}, it_max:{it_max:4d}, psnr_out(it): {psnr_out:3.2f} dB, d_out_cur: {d_out_cur:+3.2f} dB")
