# -*- coding: utf-8 -*-
"""
Collection of Light fields I/O functions.
"""

import os
import numpy as np
from re import split
from skimage.io import imread_collection, imsave


def lfread(path, ext="png", transpose=True):
    """Load a light field from a directory.

    Parameters
    ----------
    path : str
        Path to directory containing images representing
        slices of the light field.
    ext : str, optional
        File extension of images. The default is 'png'.

    Returns
    -------
    lf_array : ndarray
        Data representation of the light field as a ndarray.
        The different images are stored in the first and second dimensions, such
        that a gray-lf is VxUxYxX, an RGB-lf VxUxYxXx3 and an RGBA-lf is VxUxYxXx4.

    """

    # Get image filenames from input directory
    lightfield_path = os.path.join(path, f"*_*.{ext}")
    ic = imread_collection(lightfield_path)
    filenames = ic.files

    # Extract angular coordinates from filenames
    basename = [os.path.splitext(os.path.basename(f))[0] for f in filenames]
    u_v = [split("_", bn) for bn in basename]

    # Read and concatenate images
    lf_array = ic.concatenate()

    # Reshape light field
    uv_shape = [len(set(i)) for i in zip(*u_v)]
    lf_shape = tuple(uv_shape) + lf_array.shape[1:]
    lf_array = lf_array.reshape(lf_shape)

    # Transpose angular dimensions
    if transpose:
        lf_array = lf_array.transpose((1, 0, 2, 3, 4))

    return lf_array

def lfsave(lf_array, path, ext="png", transpose=True):
    """Save a light field to a directory.

    Parameters
    ----------
    lf_array : ndarray
        Data representation of the light field as a ndarray.
        The different images are stored in the first and second dimensions, such
        that a gray-lf is VxUxYxX, an RGB-lf VxUxYxXx3 and an RGBA-lf is VxUxYxXx4.
    path : str
        Path to directory where to save images representing
        slices of the light field.
    ext : str, optional
        File extension of images. The default is 'png'.
    transpose : bool

    Returns
    -------
    None

    """
    
    if not os.path.exists(path):
        os.makedirs(path)
    if transpose:
        lf_array = lf_array.transpose((1, 0, 2, 3, 4))
    for i in range(lf_array.shape[0]):
        for j in range(lf_array.shape[1]):
            filename = os.path.join(path, f"{i:03d}_{j:03d}.{ext}")
            imsave(filename, lf_array[i, j])
