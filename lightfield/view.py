# -*- coding: utf-8 -*-
"""
Collection of Light fields visualization functions.
"""

import os
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import ImageGrid
import lightfield as lf
from lightfield.utils import exactly_4d


def slice(lf_array, step=0.1, perm=None):
    """Display a light field using image slices.

    Parameters
    ----------
    lf_array : ndarray
        Data representation of the light field as a ndarray.
        The different images are stored in the first and second dimensions, such
        that a gray-lf is VxUxYxX, an RGB-lf VxUxYxXx3 and an RGBA-lf is VxUxYxXx4.
    ax : AxesImage, optional
        Image axes to use for plotting. The default is None.
    step: float, optional
        Time step to animate the plot in seconds. The default is 0.1.
    perm: ndarray, optional
        Axis permutation to allow different data visualizations.

    Returns
    -------
    fig : ndarray
        Figure
    ax : ndarray
        Axes

    """

    # Default: no permutation
    if perm is None:
        perm = range(lf_array.ndim)

    # Get view
    image_array, *_ = exactly_4d(lf_array.transpose(perm))

    # Initialize figure and axes
    fig = plt.figure()
    ax = plt.imshow(image_array[0])

    # Iterate along angular dimension
    for image in image_array:
        # Plot current slice
        plt.pause(step)
        ax.set_data(image)

    return fig, ax


def slices(lf_array, vuyx=None, perm=None):
    """Display a light field using image slices.

    Parameters
    ----------
    lf_array : ndarray
        Data representation of the light field as a ndarray.
        The different images are stored in the first and second dimensions, such
        that a gray-lf is VxUxYxX, an RGB-lf VxUxYxXx3 and an RGBA-lf is VxUxYxXx4.
    ax : AxesImage, optional
        Image axes to use for plotting. The default is None.
    vuyx: ndarray, optional
        Slice coordinates.
    perm: ndarray, optional
        Axis permutation to allow different data visualizations.

    Returns
    -------
    fig : ndarray
        Figure
    grid : ndarray
        Axes

    """

    # Default: first slice
    if vuyx is None:
        vuyx = [0, 0, 0, 0]

    # Default: no permutation
    if perm is None:
        perm = range(lf_array.ndim)

    # Get slices
    slices = lf.utils.slices(lf_array, vuyx=vuyx, perm=perm)

    # Initialize figure and axes
    fig = plt.figure()
    grid = ImageGrid(fig, 111, (2, 2), axes_pad=0.1, aspect=True)

    # Display slices
    for ax, im in zip(grid, slices):
        ax.imshow(im)

    plt.show(block=False)

    return fig, grid


def matrix(lf_array, perm=None):
    """Display a light field as a matrix.

    Parameters
    ----------
    lf_array : ndarray
        Data representation of the light field as a ndarray.
        The different images are stored in the first and second dimensions, such
        that a gray-lf is VxUxYxX, an RGB-lf VxUxYxXx3 and an RGBA-lf is VxUxYxXx4.
    ax : AxesImage, optional
        Image axes to use for plotting. The default is None.
    perm: ndarray, optional
        Axis permutation to allow different data visualizations.

    Returns
    -------
    fig : ndarray
        Figure
    grid : ndarray
        Axes

    """

    # Default: no permutation
    if perm is None:
        perm = range(lf_array.ndim)

    # Get view
    image, _, _, _ = lf.utils.views(lf_array, perm=perm)

    # Reshape
    shape = (np.prod(image.shape[:2]), np.prod(image.shape[2:4]), -1)
    image = np.reshape(image, shape)

    # Display
    fig = plt.figure()
    ax = plt.imshow(image, aspect="auto", interpolation="none")

    plt.show(block=False)

    return fig, ax


class View:
    def __init__(self, image, vuyx=None, perm=None) -> None:
        self.image = image
        self.vuyx = vuyx
        self.perm = perm
        return

    def slice(self, step=0.1, perm=None) -> None:
        if perm is None:
            perm = self.perm

        lf.view.slice(self.image, step=step, perm=perm)

    def slices(self, vuyx=None, perm=None) -> None:
        if vuyx is None:
            vuyx = self.vuyx

        if perm is None:
            perm = self.perm

        lf.view.slices(self.image, vuyx=vuyx, perm=perm)

    def matrix(self, perm=None) -> None:
        if perm is None:
            perm = self.perm

        lf.view.matrix(self.image, perm=perm)

def display_image(im, image, image_recon, target, image_warped, normalize=False):
    if normalize:
        image = lf.utils.to_float(image)
        image_recon = lf.utils.to_float(image_recon)
        target = lf.utils.to_float(target)
        image_warped = lf.utils.to_float(image_warped)

    if im is None:
        f, a = plt.subplots(2, 3, sharex=True, sharey=True)

        a[0][0].set_title('Target')
        a[0][1].set_title('Warped')
        a[0][2].set_title('Diff')
        a[1][0].set_title('Source')
        a[1][1].set_title('Reconstructed')
        a[1][2].set_title('Diff')

        im = [[None, None, None], [None, None, None]]
        im[0][0] = a[0][0].imshow(target)
        im[0][1] = a[0][1].imshow(image_warped)
        im[0][2] = a[0][2].imshow(0.5+target-image_warped)
        im[1][0] = a[1][0].imshow(image)
        im[1][1] = a[1][1].imshow(image_recon)
        im[1][2] = a[1][2].imshow(0.5+image-image_recon)
    else:
        im[0][0].set_data(target)
        im[0][1].set_data(image_warped)
        im[0][2].set_data(0.5+target-image_warped)
        im[1][0].set_data(image)
        im[1][1].set_data(image_recon)
        im[1][2].set_data(0.5+image-image_recon)

    plt.pause(0.1)

    return im


def display_images(im, images, images_recon, targets, images_warped, normalize=False):
    images, *_ = lf.utils.exactly_4d(images)
    images_recon, *_ = lf.utils.exactly_4d(images_recon)
    targets, *_ = lf.utils.exactly_4d(targets)
    images_warped, *_ = lf.utils.exactly_4d(images_warped)

    for (image, image_recon, target, image_warped) in zip(images, images_recon, targets, images_warped):
        im = display_image(im, image, image_recon, target,
                           image_warped, normalize)

    return im

